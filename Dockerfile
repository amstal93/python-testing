ARG PYTHON_BASE_IMAGE_VERSION

FROM python:${PYTHON_BASE_IMAGE_VERSION}-alpine

ENV PYTHONUNBUFFERED=1
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

RUN apk add --no-cache linux-headers bash gcc \
    musl-dev libjpeg-turbo-dev libpng libpq \
    postgresql-dev mysql-dev uwsgi uwsgi-python3 git \
    zlib-dev libmagic python3-dev libffi-dev \
    curl freetype-dev libxml2-dev libxslt-dev g++ \
    jpeg-dev

RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir poetry pipenv \

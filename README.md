<p align="center">
    <a href="https://www.docker.com/" target="_blank">
        <img src="https://www.docker.com/sites/default/files/mono_vertical_large.png" height="100px">
    </a>
    <h1 align="center">Docker image for testing (CI)</h1>
    <br>
</p>

**Stable** 
[![pipeline status](https://gitlab.com/exileed/python-testing/badges/master/pipeline.svg)](https://gitlab.com/exileed/python-testing/commits/master)


## About

These Docker images are built on top of the official Python Docker image (alpine), they contain additional packages required to testing (CI, but no code of the framework itself.
The `Dockerfile`(s) of this repository are designed to build from different Python-versions by using *build arguments*.

### Available versions for `exileed/python-testing`

```
3.6 3.7 3.8 3.9
```


### Available packages manager

```
poetry
pipenv
flit
```


### Example build

`docker build -t exileed/python-testing:3.6 --build-arg PYTHON_BASE_IMAGE_VERSION=3.6 .`